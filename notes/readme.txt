Notes

This subdirectory contains relevant sources for the analysis
parameters used in reanalysis_orig.R and reanalysis_revis.R , as well
as archive copies of Fig 1 as submitted with the original and revised
settings.

For reanalysis_orig.R:
  * Sources:
    - RayEtAl_article_as_published_Feb2020.pdf
    - RayEtAl_supplement_as_published_Feb2020.pdf
  * Relevant text:
    - No mention of filtering transcripts
    - "RAIN (longitudinal method)" (supplement pg 6) 

For reanalysis_revis.R:
  * Sources:
    - email_new_parameters_Jan2021.txt
  * Relevant text:
    - "Data were filtered by removing any transcript that had a zero
      FPKM value in any of the time points, and the FPKM values were
      subsequently log2 transformed."
    - "RAIN (default mode, independent method) (19) was used to detect
      rhythmic transcripts"

MD5 sums of the sources:

MD5 (RayEtAl_article_as_published_Feb2020.pdf) = ac13e5771f8897bc45261e90e934746b
MD5 (RayEtAl_supplement_as_published_Feb2020.pdf) = 0aea65ef1bf8db02f52ace50d8861283
MD5 (email_new_parameters_Jan2021.txt) = 79e7454bb9d76672b8261f5259f898fc
