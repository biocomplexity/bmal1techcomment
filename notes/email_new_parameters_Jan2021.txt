Pasted below is an excerpt of an email informing us of the new parameters.

Subject: RE: papers
From: Bryan Ray <bray@aaas.org>
Date: Sat, 9 Jan 2021 00:55:45 +0000

[...]

Ray et al. have asked for these additional changes after the review process:

"There are two additional changes in view of the Technical Comments in order to clarify methods for readers. Specifically, we’ve clarified with the highlighted bits to the sections below:

RNA-Seq data analysis
A TopHat-Cuffdiff pipeline was used to analyse the sequencing reads (37). Sequence reads were
aligned to the Ensembl built mouse reference genome (GRCm38) using TopHat version 2.1.0 (38).
Fragments Per Kilobase Million (FPKM) values were computed with Cuffdiff, normalized via the
median of the geometric means of fragment counts across all libraries as available in Cuffdiff
package (39). Data were filtered by removing any transcript that had a zero FPKM value in any of
the time points, and the FPKM values were subsequently log2 transformed.

Identification of rhythmic transcripts, proteins and phosphosites
RAIN (default mode, independent method) (19) was used to detect rhythmic transcripts, proteins
and phosphosites in wild-type and Bmal1-/- MSFs and liver tissues…"

and their original changes about the swapped data:

Draft Erratum text for the Report “Circadian rhythms in the absence of the clock gene Bmal1”

After publication of their Report, the authors discovered an error in the original manuscript. The genotypes (Bmal1+/+ and Bmal1/) in one of the datasets (liver slice time course) were inadvertently inverted during analysis. This affects some of the panels in Figs. 1 and 3 of the Report and the associated supplementary figures and tables. No other datasets or analyses are affected. These corrections do not change the conclusions of the paper.

The following items in the main text have been corrected:

1.	Fig. 1 (B, C, D): Venn diagrams and heatmaps are corrected (Bmal1+/+ and Bmal1/– panels swapped)
2.	Fig. 3 (A, B, C, D): Sequence motif analysis is included for both Bmal1+/+ and Bmal1/, as are phase distributions and rhythmic expression profiles for the ETS transcription factors. 
3.	Sentences in the main text are removed or amended to appropriately correct this genotype inversion.

Supplementary figures and tables associated with the inverted liver slice data set have also been corrected:

1.	Tables S1, S2, and S3: Data are swapped to reflect the correct genotype.
2.	Figs. S1, S2, and S3: Liver slice transcriptomics panels are swapped to reflect the correct genotype.
3.	Fig. S6: Rhythmic ETS transcription factors identified in Bmal1+/+ and Bmal1/ tissues are swapped to reflect the correct genotype. siRNA depletion analysis of the rhythmic ETS transcription factors identified in Bmal1+/+ and Bmal1/ are depicted. 
4.	Fig. S9D: Figure has been updated with corrected liver transcriptomics data.

The authors have also corrected the error in this single dataset in the Gene Expression Omnibus (GEO), accession GSE111696.

