Code and data to reproduce the analysis described in:

  Ness-Cohn, Allada, and Braun.  Comment on 'Circadian rhythms in the absense of the clock gene Bmal1'

Authors:
  Elan Ness-Cohn <elanness-cohn2017@u.northwestern.edu>
  Rosemary Braun <rbraun@northwestern.edu>

This repository is organized as follows:

.
|____R/          R scripts to carry out the analysis, with original and revised parameters
|____output/     output of the R scripts
| |____revis/      output with revised parameters
| |____orig/       output with original parameters
|____notes/      notes regarding the analysis parameters
|____data/       directory with data downloaded from GEO
| |____archive/    archival copies of the data

Note that the analysis was performed using two settings: the
parameters originally described in Ray et all in the paper as it
appeared in Jan 2020, and revised parameters that were revealed
to us in Dec 2020.  Information regarding these differences may 
be found in the readme files throughout the repository.

