Expected output from R/reanalysis_revis.R

This uses only genes with FPKM >0 at all timepoints and RAIN run using the 'independent' method.

RAINres.RData contains the data as input, the RAIN results, and session information for the run (including the package versions used).  Session information is stored in the variable runSession.

