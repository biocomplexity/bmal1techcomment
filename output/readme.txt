Expected output from R/reanalysis*.R

* orig = results using the parameters as published in the original paper [S. Ray, et al., Science 367, 800 (2020).]

* revis = results using parameters claimed in response to the technical comment

Within each subdirectory, RAINres.RData contains the data as input, the RAIN results, and session information for the run (including the package versions used).  Session information is stored in the variable runSession .

