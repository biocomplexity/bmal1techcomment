R source code

These scripts reproduce the figures from our Comment from scratch
(beginning with downloading the data from GEO).  Note that this takes
a while to execute, as RAIN needs to be run on 12 distinct datasets.

There are two versions of the analysis: the one we initially submitted
to Science, which relies on parameters in the published paper; and 
a revised analysis that relies on new parameters revealed to us in
response to our submitted comment.  As those new paramenters are not
yet published, evidence for those may be found in the notes/ directory.

The two analyses are as follows:

reanalysis_orig.R 
  runs the analysis using the parameters as published originally in
  DOI:10.1126/science.aaw7365 in Jan 2020, specifically:
  * no filtration of genes
  * RAIN "longitudinal" method

reanalysis_revis.R
  runs the analysis using the parameters revealed to us in response 
  to the originall submission of our technical comment, specifically:
  * "Data were filtered by removing any transcript that had a zero
    FPKM value in any of the time points, and the FPKM values were
    subsequently log2 transformed."
  * "RAIN (default mode, independent method) (19) was used to detect 
    rhythmic transcripts"

Output of may be found in ../output .  Note that running reanalysis.R
will overwrite those output files; you may always pull the reference
version from the git repo if needed, and copies of the combined 
figures may be found in ../notes .


