Data download directory.

Sources = supplemental files from:
  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE111696
  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE134333

Note that only the following are currently used in the reanalysis:
  GSE111696_Bmal1_MSF_genes.fpkm_tracking.txt  [experiment 1]
  GSE134333_Dataset_AM_PM_TempComp_FPKM_values.txt  [experiments 2/3]

Note that running R/reanalysis*R with "reGEO <- T" will overwrite the
files here with fresh downloads.  For posterity, the versions of the
data used in our Technical Comment are stored in archive/ , along with
their MD5 sums.
