Archive copies of data downloaded on 23 June 2020, and again on 
10 Jan 2021 (no change), as used in our submitted Technical Comment.  
These are kept in the interest of reproducibility, as those in the
parent directory may be overwritten by R/reanalysis*R

Sources = supplemental files from:
  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE111696
  https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE134333

Note that only the following are currently used in the reanalysis:
  GSE111696_Bmal1_MSF_genes.fpkm_tracking.txt  [experiment 1]
  GSE134333_Dataset_AM_PM_TempComp_FPKM_values.txt  [experiments 2/3]

MD5 sums:

MD5 (GSE111696_Bmal1_MSF_genes.fpkm_tracking.txt) = 191be57b10297f322f195fda28fc2b46
MD5 (GSE134333_Dataset_AM_PM_TempComp_FPKM_values.txt) = ce720bedaa51bd504261e4430bb11e0b
